import React, { Component } from 'react'
import Button from '../button/Button';

export default class Modal extends Component {
  render() {

    const {visible} = this.props;
    const {closeModal} = this.props

    const {text} = this.props;
    return (
     
            <div className='modal' style={{display: visible ? 'block' : 'none'}}>
            <div className='modal-container'>
            <div className='modal-header' style={{bacground: 'red', opacity: '0.5'}}>
                <h1>Do you want to delete this file?</h1>
                <span onClick={closeModal}></span>
              </div>
                <div>
                    <p>Once you delete this file, it won’t be possible to undo this action. 
                      Are you sure you want to delete it?</p>
                </div>
                <Button  className="" text={'Ok'} backgroundColor ={'grey'} onClick = {closeModal}/>
                <Button className="" text={'cancel'} backgroundColor ={'grey'} onClick = {closeModal}/>
            </div>
             
             
            </div>
      
    )
  }
}