import { Component } from "react";
import "./index.scss"
export default class Header extends Component {
  state ={
    numberStar:localStorage.getItem('numberStar'),
    numberCart:localStorage.getItem('numberCart')
  }
 componentDidMount(){
  window.addEventListener('storage', () => {
    let numberStar = localStorage.getItem('numberStar');
    let numberCart = localStorage.getItem('numberCart');
    this.setState({numberStar, numberCart})
    
})
 }
  render() {
  
  return(
    <div className="header">
    <img className="star" src="img/star.png"/>
    <p className="star-calk">{this.state.numberStar}</p>
      <img src="img/cart.png"/>
      <p className="calk">{this.state.numberCart}</p>
    </div>
  )
    
  }
}
