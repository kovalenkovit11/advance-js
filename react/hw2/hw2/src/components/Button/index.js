import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss"
export default class Button extends Component {

  render() {
   const {text, onClick, backgroundColor} = this.props
    return (
      <div className="button-container">
        <button
          className="button"
          style={{ backgroundColor }}
          type="button"
          onClick={onClick}
        >
          {text}
        </button>
      </div>
    );
  }
}
Button.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  backgroundColor: PropTypes.string,
};
Button.defaultProps = {
  backgroundColor: "#ccc",
};
