class Employee{
  constructor(name, age, salary){
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name(){
    return this._name;
  }
  set setName(name){
    this._name = name
  }
  get age(){
    return this._age;
  }
  set setAge(age){
    this._age = age
  }
  get salary(){
    return this._salary;
  }
  set setSalary(salary){
    this._salary = salary
  }

}

class Programmer extends Employee{
  constructor(name, age, salary, lang = []){

    super(name, age, salary)
    this.lang = lang
  }
  get salary(){
    return this._salary * 3;
  }
  set setSalary(salary){
    this._salary = salary * 3;
  }
}



const prog = new Programmer("andre", 26, 1200, ["eng, italian"])
 
const enginer = new Programmer("sir", 36, 2600, ["eng,ukr,tur,span"] )

console.log(prog)
console.log(enginer)