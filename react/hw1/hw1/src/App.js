import logo from './logo.svg';
import './App.css';
import Button from './components/button/Button';
import Modal from './components/modal/Modal';
import { Component } from 'react';


class App extends Component {
  state = {
		visible: false,
	}
  render(){
    const {visible} = this.state
    return (
      <div className="App">
  
       <Button text={'modal first'} backgroundColor ={'blue'} onClick = {()=>{ this.setState({
				visible: true
			})
		}}  />
       <Button text={'modal second'} backgroundColor ={'red'} onClick = {()=>{this.setState({
				visible: true
			}) }} />
       
      
      

    
        <Modal visible ={visible} closeModal = {() =>{
        this.setState({
				visible: false
       })
       }
       } />
    </div>
    );
  }

}

export default App;
