import { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";
import Button from "../Button";
import Modal from "../Modal";
export default class Card extends Component {
  state = {
    visible: false,
  };
  render() {
    const { visible } = this.state;
    // console.log(this.props);
    return (
      <div className="wrapper">
        <img src={this.props.image} />
        <div className="container-for__name">
          <h2 className="name-album">{this.props.name}</h2>
          <p className="barcode">{this.props.barcode}</p>
        </div>
        <p className="descriptions">{this.props.descriptions}</p>
        <div className="footer-cards">
          <p className="coast">{this.props.price}</p>
          <Button
            text={"Buy now"}
            onClick={() => {

              this.setState({
                visible: true,
              });
            }}
            
          />
          <Button text={"star"} onClick={()=>{
            let number =parseInt( localStorage.getItem('numberStar'))
            if (number){
              localStorage.setItem('numberStar', number + 1)
            }else { 
              localStorage.setItem('numberStar', 1)
            }
            window.dispatchEvent( new Event('storage') )

          }}/>
        </div>
        <Modal
          visible={visible}
          closeModal={() => {
            this.setState({
              visible: false,
            });
          }}
        />
      </div>
    );
  }
}
Card.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  descriptions: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  barcode: PropTypes.string.isRequired,
  color: PropTypes.string,
};
Card.defaultProps = {
  color: "#000",
};
