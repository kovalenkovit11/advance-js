import React, { Component } from 'react'

export default class Button extends Component {
  render() {

    const {text, backgroundColor, onClick } = this.props;

    return (
     
            <div className="button-container">
                <button className="button"  style={{backgroundColor }} type="button" onClick={onClick}>{text}</button>
             
            </div>
      
    )
  }
}