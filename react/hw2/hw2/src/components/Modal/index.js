import React, { Component } from "react";
import Button from "../Button";
import "./index.scss";

export default class Modal extends Component {
  render() {
    const { visible } = this.props;
    const { closeModal } = this.props;

    const { text } = this.props;
    return (
      <div className="modal" style={{ display: visible ? "block" : "none" }}>
        <div className="modal-content">
          <div
            className="modal-header"
            style={{ bacground: "red", opacity: "0.5" }}
          >
            <h1>add?</h1>
            <span onClick={closeModal}></span>
          </div>
          <div>
            <p>Did you mean?</p>
          </div>
          <div className="modal-btn">
          <Button
            className=""
            text={"Ok"}
            backgroundColor={"grey"}
            onClick={() => {
             let number = parseInt(localStorage.getItem("numberCart"));
              if (number) {
                localStorage.setItem("numberCart", number + 1);
              } else {
                localStorage.setItem("numberCart", 1);
              }
              window.dispatchEvent( new Event('storage') )

              closeModal()
            }}
          />
          <Button
            className=""
            text={"cancel"}
            backgroundColor={"grey"}
            onClick={closeModal}
          />
          </div>
        </div>
      </div>
    );
  }
}
