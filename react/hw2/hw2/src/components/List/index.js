import { Component } from "react";
import PropTypes from "prop-types";
// import './index.scss'
import Card from "./../Card/index";

export default class List extends Component {
  render() {
    // console.log(this.props.cards);
    return (
      <div className="card-wrap">
        {this.props.cards.map((card) => {
          {/* console.log(card); */}

          return (
            <Card
              name={card.name}
              price={card.price}
              image={card.image}
              barcode={card.barcode}
              color={card.color}
              descriptions={card.descriptions}
            />
          );
        })}
      </div>
    );
  }
}
List.propTypes = {
  cards: PropTypes.array,
};
