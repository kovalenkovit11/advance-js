import logo from "./logo.svg";
import "./App.css";
import Header from "./components/Header";
import { useEffect } from "react";
import Card from "./components/Card/index";
import List from "./components/List";
import { Component } from "react";
import { render } from "@testing-library/react";

export default class App extends Component {
  state = {
    cards: [],
  };
  componentDidMount() {
    fetch("data.json")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ cards: data });
      });
  }
  render() {
    return (
      <>
        <Header />
        <List cards={this.state.cards} />
      </>
    );
  }
}
